#!/usr/bin/env python3

import random


def is_valid_expression(object, function_symbols, leaf_symbols):
    """
    that takes an object as its first argument and tests whether
    it is a valid expression according to our definition of expressions
    in this assignment. The function returns True if the given object
    is valid expression, False otherwise.
    """

    if type(object) is int:
        return True
    elif type(object) is str and object in leaf_symbols:
        return True
    elif type(object) is list and len(object) == 3:
        first = object[0]
        is_first_valid = type(first) is str and first in function_symbols
        is_rest_valid = [
            is_valid_expression(object[x], function_symbols, leaf_symbols)
            for x in range(1, 3)
        ]
        return all([is_first_valid] + is_rest_valid)
    else:
        return False


def depth(expression):
    """
    takes an expression (that follows our definition of expression)
    and returns the depth of the expression tree. The depth of a
    tree is the depth of its deepest leaf.
    """

    total = 0
    if type(expression) is list:
        total += max([depth(expression[1]), depth(expression[2])])
        total += 1
    return total


def evaluate(expression, bindings):
    """
    takes an expression and a dictionary of bindings and returns an
    integer that is the value of the expression. The parameters of the function are:

    expression: an expression according to our definition of expressions;
    bindings: a dictionary where all the keys are strings and are either a function
              symbol or a variable leaf. A function symbol is mapped to a function
              that takes two arguments. A leaf symbol is mapped to an integer.
    """

    expr_type = type(expression)
    if expr_type is int:
        return expression
    elif expr_type is str:
        return bindings[expression]
    else:
        func = bindings[expression[0]]
        return func(
            evaluate(expression[1], bindings), evaluate(expression[2], bindings)
        )


def is_head():
    """
    does a coin toss and returns True if the valud is heads
    """
    return (10 * random.randint(0, 1)) > 5


def random_expression(function_symbols, leaves, max_depth):
    """
    randomly generates an expression. The function takes the following arguments:

    function_symbols: a list of function symbols (strings)
    leaves: a list of constant and variable leaves (integers and strings)
    max_depth: a non-negative integer that specifies the maximum depth allowed for
               the generated expression.
    """

    if not max_depth or is_head():
        return leaves[random.randint(0, len(leaves) - 1)]
    else:
        return [
            function_symbols[random.randint(0, len(function_symbols) - 1)],
            random_expression(function_symbols, leaves, max_depth - 1),
            random_expression(function_symbols, leaves, max_depth - 1),
        ]


def generate_rest(initial_sequence, expression, length_to_generate):
    """
    takes an initial sequence of numbers, an expression, and a specified length,
    and returns a list of integers with the specified length that is the continuation
    of the initial list according to the given expression.

    The parameters are:

    - initial_sequence: an initial sequence (list) of integer numbers that has at
                        least two numbers;
    - expression: an expression constructed from function symbols '+', '-', and
                  '*' which correspond to the three binary arithmetic functions,
                  and the leaf nodes are integers and 'x', 'y', and 'i' where the
                  intended meaning of these three symbols is described above;
    - length: a non-negative integer that specifies the length of the returned list.
    """

    results = initial_sequence[::]
    bindings = {
        "*": lambda x, y: x * y,
        "+": lambda x, y: x + y,
        "-": lambda x, y: x - y,
        "i": len(results),
        "x": results[-2],
        "y": results[-1],
    }

    for i in range(length_to_generate):
        result = evaluate(expression, bindings)
        results.append(result)

        bindings["i"] = len(results)
        bindings["x"] = results[-2]
        bindings["y"] = results[-1]

    return results[len(initial_sequence):]


def predict_rest(sequence):
    """
    takes a sequence of integers of length at least 5, finds the
    pattern in the sequence, and "predicts" the rest by returning a
    list of the next five integers in the sequence
    """
    functions = ["*", "+", "-"]
    leaves = ["x", "y", "i"] + list(range(-2, 3))

    max_attempts = 100000
    max_depth = 3

    initial_seq = sequence[:3]
    rest = sequence[3:]

    for _ in range(max_attempts):
        expr = random_expression(functions, leaves, max_depth)
        generated = generate_rest(initial_seq, expr, len(rest))
        if generated == rest:
            break
    else:
        raise Exception("No")

    return generate_rest(sequence, expr, 5)
