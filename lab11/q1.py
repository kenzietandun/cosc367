#!/usr/bin/env python3

import math

def max_value(tree):
    if type(tree) is int:
        return tree

    value = -1 * math.inf
    for t in tree:
        value = max(value, min_value(t))
    return value


def min_value(tree):
    if type(tree) is int:
        return tree

    value = math.inf
    for t in tree:
        value = min(value, max_value(t))
    return value

if __name__ == "__main__":
    game_tree = 3

    print("Root utility for minimiser:", min_value(game_tree))
    print("Root utility for maximiser:", max_value(game_tree))


    game_tree = [1, 2, 3]

    print("Root utility for minimiser:", min_value(game_tree))
    print("Root utility for maximiser:", max_value(game_tree))


    game_tree = [1, 2, [3]]

    print(min_value(game_tree))
    print(max_value(game_tree))


    game_tree = [[1, 2], [3]]

    print(min_value(game_tree))
    print(max_value(game_tree))
