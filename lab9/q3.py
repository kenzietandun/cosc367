#!/usr/bin/env python3


import csv

def learn_prior(file_name, pseudo_count=0):
    with open(file_name) as in_file:
        training_examples = [tuple(row) for row in csv.reader(in_file)] 

    trues = 0
    falses = 0
    for index, line in enumerate(training_examples):
        if index == 0:
            continue

        if int(line[-1]):
            trues += 1
        else:
            falses += 1
    
    return (trues + pseudo_count) / (trues + falses + pseudo_count * 2)

prior = learn_prior("spam-labelled.csv", 2)
print("Prior probability of spam is {:.5f}.".format(prior))
