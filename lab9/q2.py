#!/usr/bin/env python3


def normalise(value_true, value_false):
    total = value_true + value_false
    return value_true / total, value_false / total


def calculate_probability(initial_value, observation, likelihood, is_true):
    result = initial_value
    for index, obs in enumerate(observation):
        obs_likelihood = likelihood[index]
        if obs:
            result *= obs_likelihood[is_true]
        else:
            result *= 1 - obs_likelihood[is_true]
    return result


# likelihood[i][False] is p(X[i]=true|C=false)
def posterior(prior, likelihood, observation):
    true_val = calculate_probability(prior, observation, likelihood, True)
    false_val = calculate_probability(1 - prior, observation, likelihood, False)
    return normalise(true_val, false_val)[0]


prior = 0.05
likelihood = ((0.001, 0.3), (0.05, 0.9), (0.7, 0.99))

observation = (False, False, False)

class_posterior_true = posterior(prior, likelihood, observation)
print("P(C=False|observation) is approximately {:.5f}".format(1 - class_posterior_true))
print("P(C=True |observation) is approximately {:.5f}".format(class_posterior_true))
