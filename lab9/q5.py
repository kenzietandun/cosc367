#!/usr/bin/env python3 import csv

import csv


def learn_prior(file_name, pseudo_count=0):
    with open(file_name) as in_file:
        training_examples = [tuple(row) for row in csv.reader(in_file)]

    trues = 0
    falses = 0
    for index, line in enumerate(training_examples):
        if index == 0:
            continue

        if int(line[-1]):
            trues += 1
        else:
            falses += 1

    return (trues + pseudo_count) / (trues + falses + pseudo_count * 2)


def learn_likelihood(file_name, pseudo_count=0):
    with open(file_name) as in_file:
        training_examples = [tuple(row) for row in csv.reader(in_file)]

    total_lines = len(training_examples) - 1
    likelihood = [[] for x in range(len(training_examples[0]) - 1)]

    trues = 0
    falses = 0
    for index, line in enumerate(training_examples):
        if index == 0:
            continue

        for item_index, item in enumerate(line):
            # SPAM Indicator
            if item_index == len(line) - 1:
                if int(item):
                    trues += 1
                else:
                    falses += 1

            elif int(item):
                if int(line[len(line) - 1]):
                    likelihood[item_index].append(1)
                else:
                    likelihood[item_index].append(0)

    for i in range(len(likelihood)):
        likelihood[i] = calc_param_probability(
            likelihood[i], total_lines, trues, falses, pseudo_count
        )

    return likelihood


def calc_param_probability(
    param_list, total_items, total_trues, total_falses, pseudo_count
):
    result = [0, 0]
    for value in param_list:
        result[value] += 1
    result[False] = (result[False] + pseudo_count) / (total_falses + pseudo_count * 2)
    result[True] = (result[True] + pseudo_count) / (total_trues + pseudo_count * 2)
    return tuple(result)


def normalise(value_true, value_false):
    total = value_true + value_false
    return value_true / total, value_false / total


def calculate_probability(initial_value, observation, likelihood, is_true):
    result = initial_value
    for index, obs in enumerate(observation):
        obs_likelihood = likelihood[index]
        if obs:
            result *= obs_likelihood[is_true]
        else:
            result *= 1 - obs_likelihood[is_true]
    return result


# likelihood[i][False] is p(X[i]=true|C=false)
def posterior(prior, likelihood, observation):
    true_val = calculate_probability(prior, observation, likelihood, True)
    false_val = calculate_probability(1 - prior, observation, likelihood, False)
    return normalise(true_val, false_val)[0]


def nb_classify(prior, likelihood, input_vector): 
    prob = posterior(prior, likelihood, input_vector)
    label = 'Spam'
    if prob <= 0.5:
        label = 'Not Spam'
        prob = 1 - prob
    return label, prob


prior = learn_prior("spam-labelled.csv")
likelihood = learn_likelihood("spam-labelled.csv")

input_vectors = [
    (1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0),
    (0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1),
    (1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1),
    (1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1),
    (0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0),
]

predictions = [nb_classify(prior, likelihood, vector) for vector in input_vectors]

for label, certainty in predictions:
    print("Prediction: {}, Certainty: {:.5f}".format(label, certainty))
