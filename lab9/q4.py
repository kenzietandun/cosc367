#!/usr/bin/env python3

import csv
def learn_likelihood(file_name, pseudo_count=0):
    with open(file_name) as in_file: 
        training_examples = [tuple(row) for row in csv.reader(in_file)] 

    total_lines = len(training_examples) - 1
    likelihood = [[] for x in range(len(training_examples[0]) - 1)]
    
    trues = 0
    falses = 0
    for index, line in enumerate(training_examples):
        if index == 0: 
            continue

        for item_index, item in enumerate(line):
            # SPAM Indicator 
            if item_index == len(line) - 1:
                if int(item):
                    trues += 1
                else:
                    falses += 1

            elif int(item): # if X(n) is 1
                spam_value = int(line[-1])
                likelihood[item_index].append(spam_value)

    for i in range(len(likelihood)):
        likelihood[i] = calc_param_probability(likelihood[i], total_lines, trues, falses, pseudo_count)

    return likelihood


def calc_param_probability(param_list, total_items, total_trues, total_falses, pseudo_count):
    result = [0, 0]
    for value in param_list:
        result[value] += 1
    result[False] = (result[False] + pseudo_count) / (total_falses + pseudo_count * 2)
    result[True] = (result[True] + pseudo_count) / (total_trues + pseudo_count * 2)
    return tuple(result)
