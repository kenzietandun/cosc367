#!/usr/bin/env python3

def calc_probability(is_positive, positive_probability):
    if is_positive:
        return positive_probability
    return 1 - positive_probability


def get_parents(node, network):
    return network[node]["Parents"]


def get_cpt(node, network):
    return network[node]["CPT"]


def calc_prob_with_deps(node, network, assignment):
    probability = -1
    parents = get_parents(node, network)
    target_key = tuple([assignment[x] for x in parents])
    probability = calc_probability(assignment[node], get_cpt(node, network)[target_key])
    return probability


def joint_prob(network, assignment):
    probability = 1
    for node, is_positive in assignment.items():
        dependencies = get_parents(node, network)
        if not dependencies:
            positive_probability = list(get_cpt(node, network).values())[0]
            probability = probability * calc_probability(
                is_positive, positive_probability
            )
        else:
            probability = probability * calc_prob_with_deps(node, network, assignment)
    return probability


def normalise(prob_true, prob_false):
    total = prob_true + prob_false
    return (prob_true / total, prob_false / total)


def calc_query_probability(evidence, query_var, network, is_true):
    hidden_vars = network.keys() - evidence.keys() - {query_var}
    probability = 0
    for values in itertools.product((True, False), repeat=len(hidden_vars)):
        hidden_assignments = {var: val for var, val in zip(hidden_vars, values)}
        hidden_assignments[query_var] = is_true
        for e, val in evidence.items():
            hidden_assignments[e] = val
        probability += joint_prob(network, hidden_assignments)

    return probability


import itertools


def query(network, query_var, evidence):
    probability_true = calc_query_probability(evidence, query_var, network, True)
    probability_false = calc_query_probability(evidence, query_var, network, False)

    normalised_true, normalised_false = normalise(probability_true, probability_false)
    return {True: normalised_true, False: normalised_false}


network = {
    "Disease": {"Parents": [], "CPT": {(): 1 / 100000}},
    "Test": {"Parents": ["Disease"], "CPT": {(True,): 0.99, (False,): 0.01}},
}

network = {
    "Virus": {"Parents": [], "CPT": {(): 1 / 100}},
    "B": {"Parents": ["Virus"], "CPT": {(True,): 0.90, (False,): 0.05}},
    "A": {"Parents": ["Virus"], "CPT": {(True,): 0.95, (False,): 0.10}},
}

answer = query(network, "Virus", {"B": True})
print(
    "The probability of carrying the virus\n"
    "if test B is positive: {:.5f}".format(answer[True])
)
