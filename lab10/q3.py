#!/usr/bin/env python3


def construct_perceptron(weights, bias):
    """Returns a perceptron function using the given paramers."""

    def perceptron(input):
        # Complete (a line or two)

        a = sum([weight * i for weight, i in zip(weights, input)])

        # Note: we are masking the built-in input function but that is
        # fine since this only happens in the scope of this function and the
        # built-in input is not needed here.
        return 1 if a + bias >= 0 else 0

    return perceptron  # this line is fine


def accuracy(classifier, inputs, expected_outputs):
    """
    passes each input in the sequence of inputs to the given classifier
    function (e.g. a perceptron) and compares the predictions with
    the expected outputs. The function must return the accuracy of
    the classifier on the given data. Accuracy must be a number between
    0 and 1 (inclusive).

    Note: an important application of a metric such as accuracy is
    to see how a classifier (e.g. a spam filter) performs on unseen
    data. In this case, the inputs must be some data that it has
    not seen during training but has been labeled by humans.
    """

    classified = [classifier(i) for i in inputs]
    similars = [1 for i, j in zip(classified, expected_outputs) if i == j]
    return sum(similars) / len(classified)


def update_weights(old_weights, eta, inputs, target_diff):
    new_weights = []
    for i in range(len(old_weights)):
        inp = inputs[i]
        weight = old_weights[i]
        new_weights.append(weight + eta * inp * target_diff)
    return new_weights


def update_bias(old_bias, eta, target_diff):
    return old_bias + eta * target_diff


def learn_perceptron_parameters(
    weights, bias, training_examples, learning_rate, max_epochs
):
    """
    adjusts the weights and bias by iterating through the training
    data and applying the perceptron learning rule. The function
    must return a pair (2-tuple) where the first element is the vector
    (list) of adjusted weights and second argument is the adjusted
    bias. The parameters of the function are:

    weights: an array (list) of initial weights of length n

    bias: a scalar number which is the initial bias

    training_examples: a list of training examples where each example
    is a pair. The first element of the pair is a vector (tuple)
    of length n. The second element of the pair is an integer which
    is either 0 or 1 representing the negative or positive class
    correspondingly.

    learning_rate: a positive number representing eta in the learning
    equations of perceptron.

    max_epochs: the maximum number of times the learner is allowed
    to iterate through all the training examples.
    """

    for _ in range(max_epochs):
        for example, target in training_examples:
            perceptron = construct_perceptron(weights, bias)
            output = perceptron(example)
            if output != target:
                # Update the weight
                target_diff = target - output
                weights = update_weights(weights, learning_rate, example, target_diff)
                bias = update_bias(bias, learning_rate, target_diff)
            print(f"{weights=}")
            print(f"{bias=}")
    return weights, bias


weights = [-0.5, 0.5]
bias = -0.5
learning_rate = 0.5

examples = [
    ([1, 1],   0),    # index 0 (first example)
    ([2, 0],   1),
    ([1, -1],  0),
    ([-1, -1], 1),
    ([-2, 0],  0),
    ([-1, 1],  1),
]

w,b = learn_perceptron_parameters(weights,bias,examples,learning_rate,1)
print(f"{w=}")
print(f"{b=}")
