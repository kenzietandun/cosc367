#!/usr/bin/env python3

import math


def euclidean_distance(v1, v2):
    """
    v1 and v2 are two numeric vectors (sequences) with the same number of elements.
    The function must return the euclidean distance between the points represented by v1 and v2
    """

    return math.sqrt(sum([(x - y) ** 2 for x, y in zip(v1, v2)]))


def majority_element(labels):
    """
    majority_element(labels) where labels is a collection of class labels.
    The function must return a label that has the highest frequency (most common).
    [if there is a tie it doesn't matter which majority is returned.]
    """

    count = dict()
    for label in labels:
        count[label] = count.get(label, 0) + 1

    most_frequent_label = None
    max_count = max(count.values())
    
    for k, v in count.items():
        if v == max_count:
            if not most_frequent_label:
                most_frequent_label = k
            elif k < most_frequent_label:
                most_frequent_label = k

    return most_frequent_label


def knn_predict(input, examples, distance, combine, k):
    """
    knn_predict(input, examples, distance, combine, k) that takes
    an input and predicts the output by combining the output of the
    k nearest neighbours. If after selecting k nearest neighbours,
    the distance to the farthest selected neighbour and the distance
    to the nearest unselected neighbour are the same, more neighbours
    must be selected until these two distances become different or
    all the examples are selected. The description of the parameters
    of the function are as the following:

       input: an input object whose output must be predicted. Do
       not make any assumption about the type of input other than that
       it can be consumed by the distance function.

       examples: a collection of pairs. In each pair the first element
       is of type input and the second element is of type output.

       distance: a function that takes two objects and returns a non-negative
       number that is the distance between the two objects according
       to some metric.

       combine: a function that takes a set of outputs and combines
       them in order to derive a new prediction (output).

       k: a positive integer which is the number of nearest neighbours
       to be selected. If there is a tie more neighbours will be selected
       (see the description above).

    Note: the majority_element function used in some test cases returns
    the smallest element when there is a tie. For example majority_element('--++')
    returns '+' because it is the most common label (like -) and
    in the character encoding system '+' comes before '-'.
    """

    examples_copy = []
    for item in examples:
        input_type, output_type = item
        examples_copy.append((item, distance(input, input_type)))

    by_distance = sorted(examples_copy, key=lambda x: x[1])
    furthest_distance = by_distance[k - 1][1]
    by_distance = [x[1] for x, dist in by_distance if dist <= furthest_distance]

    return combine(by_distance)

