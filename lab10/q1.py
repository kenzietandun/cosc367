#!/usr/bin/env python3

import math

def euclidean_distance(v1, v2):
    '''
    v1 and v2 are two numeric vectors (sequences) with the same number of elements. 
    The function must return the euclidean distance between the points represented by v1 and v2
    '''

    return math.sqrt(sum([(x-y)**2 for x, y in zip(v1, v2)]))

def majority_element(labels):
    '''
    majority_element(labels) where labels is a collection of class labels. 
    The function must return a label that has the highest frequency (most common). 
    [if there is a tie it doesn't matter which majority is returned.] 
    '''

    most_frequent = 0
    most_frequent_label = None
    count = dict()
    for label in labels:
        count[label] = count.get(label, 0) + 1

        if count[label] > most_frequent:
            most_frequent = count[label]
            most_frequent_label = label

    return most_frequent_label
