#!/usr/bin/env python3

import re


def clauses(knowledge_base):
    """Takes the string of a knowledge base; returns an iterator for pairs
    of (head, body) for propositional definite clauses in the
    knowledge base. Atoms are returned as strings. The head is an atom
    and the body is a (possibly empty) list of atoms.

    -- Kourosh Neshatian - 31 Jul 2019

    """
    ATOM = r"[a-z][a-zA-z\d_]*"
    HEAD = rf"\s*(?P<HEAD>{ATOM})\s*"
    BODY = rf"\s*(?P<BODY>{ATOM}\s*(,\s*{ATOM}\s*)*)\s*"
    CLAUSE = rf"{HEAD}(:-{BODY})?\."
    KB = rf"^({CLAUSE})*\s*$"

    assert re.match(KB, knowledge_base)

    for mo in re.finditer(CLAUSE, knowledge_base):
        yield mo.group("HEAD"), re.findall(ATOM, mo.group("BODY") or "")

def forward_deduce(knowledge_base):
    """
    takes the string of a knowledge base containing 
    propositional definite clauses and returns a (complete) 
    set of atoms (strings) that can be derived (to be true) 
    from the knowledge base.
    """
    rules = list(clauses(knowledge_base))
    
    atoms = set()
    while True:
        added_atom = []
        for head, body in rules:
            if not body or set(body).issubset(atoms):
                atoms.add(head)
                added_atom.append((head, body))
        if added_atom:
            for a in added_atom:
                rules.remove(a)
        else:
            break

    return atoms

def main():
    
    kb = """
    a :- b.
    b.
    """

    print(", ".join(sorted(forward_deduce(kb))))

    
    kb = """
    good_programmer :- correct_code.
    correct_code :- good_programmer.
    """

    print(", ".join(sorted(forward_deduce(kb))))

        
    kb = """
    a :- b, c.
    b :- d, e.
    b :- g, e.
    c :- e.
    d.
    e.
    f :- a,
         g.
    """

    print(", ".join(sorted(forward_deduce(kb))))

if __name__ == "__main__":
    main()
	
