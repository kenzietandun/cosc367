#!/usr/bin/env python3

import heapq
import math

from search import Arc, Frontier, Graph, generic_search, print_actions

# fmt: off
movements = [
        ('N', -1,  0),
        ('E',  0,  1),
        ('S',  1,  0),
        ('W',  0, -1),
]
# fmt: on


class AStarFrontier(Frontier):
    def __init__(self, map_graph):
        self.map_graph = map_graph
        self.heap = []
        self.visited_paths = set()

    def add(self, path):
        if path[-1].head in self.visited_paths:
            return
        heapq.heappush(self.heap, (self._calculate_total_cost(path), path))

    def _calculate_total_cost(self, path):
        total_cost = sum([x.cost for x in path])
        total_cost += self.map_graph.estimated_cost_to_goal(path[-1].head)

        direction = path[-1].action
        direction_priority = 0
        for i, move in enumerate(movements):
            if move[0] == direction:
                direction_priority = -i
                break

        return (total_cost, len(path), direction_priority)

    def __iter__(self):
        return self

    def __next__(self):
        while True:
            if not self.heap:
                raise StopIteration
            next_path = heapq.heappop(self.heap)[1]

            if next_path[-1].head:
                if next_path[-1].head not in self.visited_paths:
                    self.visited_paths.add(next_path[-1].head)
                    break
        return next_path

class RoutingGraph(Graph):
    def __init__(self, map_str):
        self.MAX_FUEL = 9
        self.map_str = map_str.strip()
        self.grid = [list(x.strip()) for x in self.map_str.split("\n")]
        self.height = len(self.grid)
        self.width = len(self.grid[0])

    def _is_wall(self, row, col):
        return row == 0 or col == 0 or row == self.height - 1 or col == self.width - 1

    def _is_obstacle(self, row, col):
        return self.grid[row][col] == "X"

    def _is_pathable(self, row, col):
        return not (self._is_wall(row, col) or self._is_obstacle(row, col))

    def is_goal(self, node):
        row, col, _ = node
        return self.grid[row][col] == "G"

    def _is_fuel_node(self, row, col):
        return self.grid[row][col] == "F"

    def starting_nodes(self):
        starting_nodes = []
        for row in range(self.height):
            for col in range(self.width):
                node = self.grid[row][col]
                if node.isdigit():
                    starting_nodes.append((row, col, int(node)))
                if node == "S":
                    starting_nodes.append((row, col, math.inf))
        return starting_nodes

    def outgoing_arcs(self, tail_node):
        row, col, fuel = tail_node

        arcs = []

        if fuel:
            for direction, y, x in movements:
                tgt_row = row + y
                tgt_col = col + x
                next_state = (tgt_row, tgt_col, fuel - 1)
                if self._is_pathable(tgt_row, tgt_col):
                    arcs.append(Arc(tail=tail_node, head=next_state, action=direction, cost=5))

        if self._is_fuel_node(row, col) and fuel < self.MAX_FUEL:
            next_state = (row, col, self.MAX_FUEL)
            arcs.append(Arc(tail=tail_node, head=next_state, action="Fuel up", cost=15))

        return arcs

    def estimated_cost_to_goal(self, node):
        return 0


if __name__ == "__main__":
    #    map_str = """\
    # +-------+
    # |   G   |
    # |       |
    # |   S   |
    # +-------+
    # """
    #
    #    map_graph = RoutingGraph(map_str)
    #    frontier = AStarFrontier(map_graph)
    #    solution = next(generic_search(map_graph, frontier), None)
    #    print_actions(solution)
    #
    #    map_str = """\
    # +-------+
    # |  GG   |
    # |S    G |
    # |  S    |
    # +-------+
    # """
    #
    #    map_graph = RoutingGraph(map_str)
    #    frontier = AStarFrontier(map_graph)
    #    solution = next(generic_search(map_graph, frontier), None)
    #    print_actions(solution)

    map_str = """\
+---------+
|2        |
|    G 123|
|2XXXXXXX |
| FF      |
+---------+
"""

    map_str = """\
+-------+
|  F  X |
|X XXXXG|
| 3     |
+-------+
"""
#    map_str = """\
#+-------+
#|       |
#|XSXXXXG|
#|       |
#+-------+
#"""
#    map_str = """\
#+----------+
#|    X     |
#| S  X  G  |
#|    X     |
#+----------+
#"""

    map_str = """\
+----------------+
|2              F|
|XX     G 123    |
|3XXXXXXXXXXXXXX |
|  F             |
|          F     |
+----------------+
"""


#    map_str = """\
#+-------------+
#|         G   |
#| S           |
#|         S   |
#+-------------+
#"""

#    map_str = """\
#+-----+
#|S    |
#|     |
#|     |
#|     |
#|     |
#|2  G |
#| F   |
#+-----+
#"""
#    map_str = """\
#+----------+
#| S4    G  |
#+----------+
#"""
    map_graph = RoutingGraph(map_str)
    frontier = AStarFrontier(map_graph)
    solution = next(generic_search(map_graph, frontier), None)
    print_actions(solution)
