#!/usr/bin/env python3
import math

from search import *
import heapq


class LocationGraph(ExplicitGraph):
    def __init__(self, nodes, locations, edges, starting_nodes, goal_nodes):
        self.nodes = nodes
        self.locations = locations
        self.edges = edges
        self._starting_nodes = starting_nodes
        self.goal_nodes = goal_nodes

    def _get_distance(self, node1, node2):
        node1_x, node1_y = self.locations[node1]
        node2_x, node2_y = self.locations[node2]

        return math.sqrt((node2_x - node1_x) ** 2 + (node2_y - node1_y) ** 2)

    def starting_nodes(self):
        return self._starting_nodes

    def is_goal(self, node):
        """Returns true if the given node is a goal node."""
        return node in self.goal_nodes

    def _find_neighbours(self, node):
        neighbors = []
        for edge in self.edges:
            n1, n2 = edge
            if n1 == node:
                neighbors.append(n2)
            elif n2 == node:
                neighbors.append(n1)
        return neighbors

    def outgoing_arcs(self, node):
        arcs = set()
        neighbors = self._find_neighbours(node)
        for n in neighbors:
            arcs.add(Arc(node, n, action=f"{node}->{n}", cost=self._get_distance(n, node)))
        return sorted(arcs)


class LCFSFrontier(Frontier):
    def __init__(self):
        self.heap = []

    def add(self, path):
        total_cost = sum([x.cost for x in path])
        heapq.heappush(self.heap, (total_cost, path))

    def __iter__(self):
        """The object returns itself because it is implementing a __next__
        method and does not need any additional state for iteration."""
        return self

    def __next__(self):
        if len(self.heap) > 0:
            return heapq.heappop(self.heap)[1]
        raise StopIteration


def main():
    graph = LocationGraph(
        nodes=set("ABC"),
        locations={"A": (0, 0), "B": (3, 0), "C": (3, 4)},
        edges={("A", "B"), ("B", "C"), ("B", "A"), ("C", "A")},
        starting_nodes=["A"],
        goal_nodes={"C"},
    )

    solution = next(generic_search(graph, LCFSFrontier()))
    print_actions(solution)

    pythagorean_graph = LocationGraph(
        nodes=set("abc"),
        locations={"a": (5, 6), "b": (10, 6), "c": (10, 18)},
        edges={tuple(s) for s in {"ab", "ac", "bc"}},
        starting_nodes=["a"],
        goal_nodes={"c"},
    )

    solution = next(generic_search(pythagorean_graph, LCFSFrontier()))
    print_actions(solution)

    graph = LocationGraph(
        nodes=set("ABC"),
        locations={"A": (0, 0), "B": (3, 0), "C": (3, 4)},
        edges={("A", "B"), ("B", "C"), ("B", "A")},
        starting_nodes=["A"],
        goal_nodes={"C"},
    )

    solution = next(generic_search(graph, LCFSFrontier()))
    print_actions(solution)


if __name__ == "__main__":
    main()

